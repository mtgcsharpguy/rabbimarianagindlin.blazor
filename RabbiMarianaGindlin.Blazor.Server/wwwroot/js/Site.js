﻿function getSomeGoodOldStuff() {
    var dom='&#x68;&#x6F;&#x74;&#x6D;&#x61;&#x69;&#x6C;.&#x63;&#x6F;&#x6D;',
        display='&#x4D;&#x61;&#x72;&#x69;&#x61;&#x6E;&#x61; &#x47;&#x69;&#x6E;&#x64;&#x6C;&#x69;&#x6E;',
        x40='&#x40;',
        part1='&#x6D;&#x61;&#x69;&#x6C;&#x74;&#x6F;:',
        name='&#x73;&#x68;&#x65;&#x69;&#x6E;&#x64;&#x65;&#x6C;&#x65;&#x30;&#x31;';
    
    return (part1 + name + x40 + dom);
}

$(function () {
    var stuff = '<a id="' + $("#navigationItemLinkContact").attr("id") + '" class="' + $("#navigationItemLinkContact").attr("class") + '" href="' + getSomeGoodOldStuff() + '">Contact</a>';

    $("#navigationItemLinkContact").replaceWith($(stuff));
});

window.jPlayerInitPlaylist = function (music) {
    var cssSelector = {
        jPlayer: "#jquery_jplayer_" + music.ordinal, cssSelectorAncestor: "#jp_container_" + music.ordinal
    };
    console.log(music);
    var playlist = music.playlist;

    var options = {
        swfPath: "/js/jPlayer", supplied: "mp3", errorAlerts: true, error: function (event) {
            console.log(JSON.stringify(event.jPlayer.error));
            if (event.jPlayer.error.type === $.jPlayer.error.URL_NOT_SET) {
                myPlaylist1.next();
            }
        }
    };

    var myPlaylist1 = new jPlayerPlaylist(cssSelector, playlist, options);
};

window.clearContactForm = function (message) {
    var submitButtonText = $("#contactSubmitButton").text();

    $("#contactSubmitButton").attr("disabled", "disabled");
    $("#contactSubmitButton").text(message);
    $("#fromEmail").val("");
    $("#fromBody").val("");

    var restoreContactForm = function () {
        $("#contactSubmitButton").text(submitButtonText);
        $("#contactSubmitButton").removeAttr("disabled");
    };

    this.setTimeout(restoreContactForm, 10000);
};