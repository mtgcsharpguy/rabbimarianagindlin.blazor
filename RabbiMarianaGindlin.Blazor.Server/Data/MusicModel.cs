﻿using System;

namespace RabbiMarianaGindlin.Blazor.Server.Data
{
    [Serializable]
    public class MusicModel
    {
        public int Ordinal { get; set; }
        public string Title { get; set; }
        public string CoverImage { get; set; }
        public MusicPlayList[] Playlist { get; set; }
    }

    public class MusicPlayList
    {
        public string title { get; set; }
        public string mp3 { get; set; }
    }
}
