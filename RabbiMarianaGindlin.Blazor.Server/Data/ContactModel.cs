﻿using System.ComponentModel.DataAnnotations;

namespace RabbiMarianaGindlin.Blazor.Server.Data
{
    public class ContactModel
    {
        [Required(ErrorMessage = "Your email is required.")]
        [RegularExpression("^(([^<>()\\[\\]\\\\.,;:\\s@\"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@\"]+)*)|(\".+\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$", ErrorMessage = "Your email is not valid.")]
        public string FromEmail { get; set; }
        [Required(ErrorMessage = "Your message is required.")]
        public string FromBody { get; set; }
    }
}
