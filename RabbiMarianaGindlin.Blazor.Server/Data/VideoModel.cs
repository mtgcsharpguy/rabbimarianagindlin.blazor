﻿using System;

namespace RabbiMarianaGindlin.Blazor.Server.Data
{
    [Serializable]
    public class VideoModel
    {
        public string Title { get; set; }
        public string URL { get; set; }
    }
}
