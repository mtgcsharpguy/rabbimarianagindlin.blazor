﻿using Microsoft.Extensions.Configuration;
using RabbiMarianaGindlin.Blazor.Server.Data;
using System;
using System.Net.Mail;

namespace RabbiMarianaGindlin.Blazor.Server.Service
{
    public class ContactService : IContactService
    {
        IConfiguration _configuration;

        public ContactService(IConfiguration configuration)
        {
            _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
        }

        public void SendEmailMessage(ContactModel contactInfo)
        {
            var smtpServer = _configuration["SMTPServer"];
            var toEMail = _configuration["EMail"];
            var fromEmail = contactInfo.FromEmail;
            var messageBody = contactInfo.FromBody;

            using (var smtpClient = new SmtpClient(smtpServer))
            {
                using (var mailMessage = new MailMessage(fromEmail, toEMail, "Website User Message", messageBody))
                {
                    smtpClient.Send(mailMessage);
                }
            }
        }
    }
}
