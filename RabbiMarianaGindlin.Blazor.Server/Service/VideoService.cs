﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RabbiMarianaGindlin.Blazor.Server.Data;
using System;
using System.IO;
using System.Threading.Tasks;

namespace RabbiMarianaGindlin.Blazor.Server.Service
{
    public class VideoService : IVideoService
    {
        private readonly IWebHostEnvironment _webHostEnvironment;
        private readonly IConfiguration _configuration;

        public VideoService(IWebHostEnvironment webHostEnvironment, IConfiguration configuration)
        {
            _webHostEnvironment = webHostEnvironment ?? throw new ArgumentNullException(nameof(webHostEnvironment));
            _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
        }

        public Task<VideoModel[]> GetVideoAsync()
        {
            var VideoJSON = File.ReadAllText($"{ _webHostEnvironment.ContentRootPath }{ _configuration.GetSection("Data")["Video"] }");
            var Video = JsonConvert.DeserializeObject<VideoModel[]>(VideoJSON);

            var result = Task.FromResult(Video);
            return result;
        }
    }
}
