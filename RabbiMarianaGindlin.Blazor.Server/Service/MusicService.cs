﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RabbiMarianaGindlin.Blazor.Server.Data;
using System;
using System.IO;
using System.Threading.Tasks;

namespace RabbiMarianaGindlin.Blazor.Server.Service
{
    public class MusicService : IMusicService
    {
        private readonly IWebHostEnvironment _webHostEnvironment;
        private readonly IConfiguration _configuration;

        public MusicService(IWebHostEnvironment webHostEnvironment, IConfiguration configuration)
        {
            _webHostEnvironment = webHostEnvironment ?? throw new ArgumentNullException(nameof(webHostEnvironment));
            _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
        }

        public Task<MusicModel[]> GetMusicAsync()
        {
            var musicJSON = File.ReadAllText($"{ _webHostEnvironment.ContentRootPath }{ _configuration.GetSection("Data")["Music"] }");
            var music = JsonConvert.DeserializeObject<MusicModel[]>(musicJSON);

            var result = Task.FromResult(music);
            return result;
        }
    }
}
