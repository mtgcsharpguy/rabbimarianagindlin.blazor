﻿using RabbiMarianaGindlin.Blazor.Server.Data;
using System.Threading.Tasks;

namespace RabbiMarianaGindlin.Blazor.Server.Service
{
    public interface IMusicService
    {
        Task<MusicModel[]> GetMusicAsync();
    }
}
