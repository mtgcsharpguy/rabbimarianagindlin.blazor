﻿using RabbiMarianaGindlin.Blazor.Server.Data;
using System.Threading.Tasks;

namespace RabbiMarianaGindlin.Blazor.Server.Service
{
    public interface INewsService
    {
        Task<NewsModel[]> GetNewsAsync();
    }
}
