﻿using RabbiMarianaGindlin.Blazor.Server.Data;

namespace RabbiMarianaGindlin.Blazor.Server.Service
{
    public interface IContactService
    {
        void SendEmailMessage(ContactModel contactInfo);
    }
}
