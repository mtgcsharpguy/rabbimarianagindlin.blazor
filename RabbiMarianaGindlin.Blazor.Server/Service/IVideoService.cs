﻿using RabbiMarianaGindlin.Blazor.Server.Data;
using System.Threading.Tasks;

namespace RabbiMarianaGindlin.Blazor.Server.Service
{
    public interface IVideoService
    {
        Task<VideoModel[]> GetVideoAsync();
    }
}
