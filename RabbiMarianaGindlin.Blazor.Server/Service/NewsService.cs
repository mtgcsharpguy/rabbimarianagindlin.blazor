﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RabbiMarianaGindlin.Blazor.Server.Data;
using System;
using System.IO;
using System.Threading.Tasks;

namespace RabbiMarianaGindlin.Blazor.Server.Service
{
    public class NewsService : INewsService
    {
        private readonly IWebHostEnvironment _webHostEnvironment;
        private readonly IConfiguration _configuration;

        public NewsService(IWebHostEnvironment webHostEnvironment, IConfiguration configuration)
        {
            _webHostEnvironment = webHostEnvironment ?? throw new ArgumentNullException(nameof(webHostEnvironment));
            _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
        }

        public Task<NewsModel[]> GetNewsAsync()
        {
            var NewsJSON = File.ReadAllText($"{ _webHostEnvironment.ContentRootPath }{ _configuration.GetSection("Data")["News"] }");
            var News = JsonConvert.DeserializeObject<NewsModel[]>(NewsJSON);

            var result = Task.FromResult(News);
            return result;
        }
    }
}
